#! /usr/bin/env python3

import sys, base64, os, argparse, glob, re, logging, json
from binascii import hexlify
from datetime import datetime

from cryptography import x509
from cryptography.hazmat.backends import default_backend


a_p = argparse.ArgumentParser(
    description=\
"""Aus einer angegeben TSL-Datei suche ich die CA und die dazugehörigen
OCSP-Responder Zertifikate zusammen für (oid_egk_aut, oid_egk_aut_alt,
oid_smc_b_aut) und geben sie im SGDCertificateImportList-Format aus."""
)
a_p.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='verbose mode')
a_p.add_argument(dest='TSL_Datei', metavar='TSL-Datei')
args = a_p.parse_args()

logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
                    datefmt="%Y-%m-%dT%H:%M:%S%z")

dirs = sorted(glob.glob("certs/*"))

vl = { "meta" : { "iat" : datetime.now().isoformat(), "prev" : "" },
       "egk"  : [ ],
       "hba"  : [ ],
       "internet" : [ ],
       "roots"  : [ ],
       "bnetza" : [ ],
       "ocsp" : []
     }

print(json.dumps(vl, indent=4))

#root_cert_files = sorted(glob.glob("GEM.RCA*.der"))
#root_ids = []
#root_ocsp_files = sorted(glob.glob("OCSP-GEM.RCA*.der"))
#roots_info = []
#
#ausgabe = ('<?xml version = "1.0" encoding = "ISO-8859-1" ?>\n' +
#           "<!-- Date: " + datetime.now().isoformat() + 
#           " File: " + args.TSL_Datei + " -->\n\n" +
#           "<SGDCertificateImportList>\n")
#
#def parse_cert(cert: x509.Certificate):
#    """
#    Ermittelt Informationen aus dem übergebenen Zertifikat und gibt diese
#    als dictionary zurück.
#    """
#
#    #CertName = ', '.join(x for x in [y.value for y in cert.subject]) 
#    CertName = ', '.join(x for x in [y.rfc4514_string() for y in cert.subject]) 
#    CertID = ""
#    for i in cert.subject:
#        if i.rfc4514_string().startswith("CN="):
#            cn = i.rfc4514_string()[3:]
#            CertID = hexlify(cn.encode()).decode()
#
#    for ext in cert.extensions:
#        if ext.oid == x509.oid.ExtensionOID.SUBJECT_KEY_IDENTIFIER:
#            #print(e.oid)
#            subj_k_i = hexlify(ext.value.digest).decode()
#            #print(subj_k_i)
#            if config["brainpoolOpenSSL"]:
#                test_1 = x509.SubjectKeyIdentifier.from_public_key(cert.public_key())
#                if ext.value.digest != test_1.digest:
#                    logging.warning("Keyidentifyer nicht RFC konform?")
#    assert subj_k_i
#    assert CertID
#
#    a_k_i = ""
#    for ext in cert.extensions:
#        if ext.oid == x509.oid.ExtensionOID.AUTHORITY_KEY_IDENTIFIER:
#            a_k_i = hexlify(ext.value.key_identifier).decode()
#
#    return {"subj_k_i" : subj_k_i, 
#            "cn"       : cn,
#            "CertName" : CertName,
#            "CertID"   : CertID,
#            "a_k_i"    : a_k_i,
#            "NotValidAfter" : cert.not_valid_after.isoformat(timespec='minutes')
#           }
#
#for i in root_cert_files:
#    with open(i, "rb") as f:
#        cert_der = f.read()
#        cert = x509.load_der_x509_certificate(cert_der, default_backend())
#        cert_info = parse_cert(cert)
#        roots_info.append(cert_info)
#        ausgabe += (f"""
#<SGDCertificate>
#    <CertName>{cert_info["CertName"]}</CertName>
#    <CertType>Root</CertType>
#    <CertID>{cert_info["CertID"]}</CertID>
#    <CertValue>{hexlify(cert_der).decode()}</CertValue>
#</SGDCertificate>\n""")
#
#for i in root_ocsp_files:
#    with open(i, "rb") as f:
#        cert_der = f.read()
#        cert = x509.load_der_x509_certificate(cert_der, default_backend())
#        cert_info = parse_cert(cert)
#        ausgabe += (f"""
#<SGDCertificate>
#    <CertName>{cert_info["CertName"]}</CertName>
#    <CertType>OCSPSigner</CertType>
#    <CertID>{cert_info["CertID"]}</CertID>
#    <CertValue>{hexlify(cert_der).decode()}</CertValue>
#    <OCSPSigneeList>""")
#        assert len(roots_info)>0
#        for root in roots_info:
#            ausgabe += (f"""
#        <OCSPSignee>
#            <CertName>{root["CertName"]}</CertName>
#            <CertID>{root["CertID"]}</CertID>
#        </OCSPSignee>""")
#
#        ausgabe += "\n    </OCSPSigneeList>\n</SGDCertificate>\n"
#
#ns = {'tsl': 'http://uri.etsi.org/02231/v2#'}
#
#xml_doc_root = ElementTree.parse(args.TSL_Datei).getroot()
#if not xml_doc_root.tag.endswith('TrustServiceStatusList'):
#    sys.exit("keine gültige TI-TSL")
#
#if 'Id' in xml_doc_root.attrib:
#    logging.info('TSL-id: {}'.format(xml_doc_root.attrib['Id']))
#else:
#    sys.exit("keine gültige TI-TSL")
#
#c1 = xml_doc_root.findall('./tsl:SchemeInformation/tsl:ListIssueDateTime', ns)
#logging.info("TSL erzeugt am " + c1[0].text)
#if datetime.now() < datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ'):
#    logging.critical("lokale Zeit ist falsch.")
#c1 = xml_doc_root.findall('./*/tsl:NextUpdate/tsl:dateTime', ns)
#logging.info("nächstes Update " + c1[0].text)
#if datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ') < datetime.now():
#    logging.critical("TSL-Gültigkeit abgelaufen oder lokale Zeit ist falsch.")
#
#
#TSP_Counter=0; TSP_Service_Counter=0; Zertifikate_Gefunden_Counter=0
#Suchziele = ["oid_egk_aut", "oid_egk_aut_alt", "oid_smc_b_aut"]
#
#TSP_Liste=xml_doc_root.findall('./tsl:TrustServiceProviderList', ns)
#for TSP in TSP_Liste[0]:
#    TSP_Counter += 1
#    tn=TSP.findall('./*/tsl:TSPName/tsl:Name', ns)
#    logging.info("---------- " + tn[0].text)
#    TSP_Services=TSP.findall('./tsl:TSPServices', ns)[0]
#    CA_Liste=[]
#    OCSP_Liste=[]
#    for TSP_Service in TSP_Services.findall('./tsl:TSPService/', ns):
#        TSP_Service_Counter += 1
#        STI=TSP_Service.findall('./tsl:ServiceTypeIdentifier', ns)[0]
#        logging.debug(STI.text)
#        if STI.text.endswith("/CA/PKC"):
#            Extensions=TSP_Service.findall('./tsl:ServiceInformationExtensions', ns)[0]
#            Gefunden=False
#            for ex in Extensions:
#                for ee in ex:
#                    if not ee.text:
#                        continue
#                    wert=ee.text.strip()
#                    if wert in Suchziele:
#                        Gefunden=True
#                        break
#                if Gefunden:
#                    break
#            if Gefunden:
#                logging.info("Ziel gefunden")
#
#            if Gefunden:
#                Zertifikate_Gefunden_Counter += 1
#                Erfolgreich_Extrahiert = False
#                for i in TSP_Service.findall('./tsl:ServiceDigitalIdentity/tsl:DigitalId', ns):
#                    for a in i:
#                        if a.tag == '{http://uri.etsi.org/02231/v2#}Other':
#                            x = a[0]
#                            data = x.text.strip()
#                        else:
#                            data = a.text.strip()
#
#                        assert len(data)>0
#                        data_bin = base64.b64decode(data)
#                        #with open(str(Zertifikate_Gefunden_Counter)+'.der', "wb") as f:
#                        #    f.write(data)
#                        #    Erfolgreich_Extrahiert=True
#
#                        cert = x509.load_der_x509_certificate(data_bin, default_backend())
#                        cert_info = parse_cert(cert)
#                        cert_info["cert.hex"] = hexlify(data_bin).decode()
#                        CA_Liste.append(cert_info)
#
#                        Erfolgreich_Extrahiert=True
#                        
#                if not Erfolgreich_Extrahiert:
#                    sys.exit("Fehler bei der Extraktion.")
#        elif STI.text.endswith("/Certstatus/OCSP"):
#            search_res = TSP_Service.findall('./tsl:ServiceDigitalIdentity/tsl:DigitalId', ns)
#            assert search_res
#            data = search_res[0][0].text
#            data_bin = base64.b64decode(data)
#            cert = x509.load_der_x509_certificate(data_bin, default_backend())
#            cert_info = parse_cert(cert)
#            cert_info["cert.hex"] = hexlify(data_bin).decode()
#            OCSP_Liste.append(cert_info)
#        else:
#            logging.debug("Ich ignoriere " + STI.text)
#
#    # jetzt habe ich die komplette Ziel-CA-Liste und die zugehörige OCSP-Liste
#    # wenn die CA-Liste leer ist gibt es für diesen TSP nichts zu tun 
#    # (bspw. Komponenten-PKI) 
#    if len(CA_Liste) == 0:
#        continue
#
#    for cert_info in CA_Liste:
#        ausgabe += (f"""
#<SGDCertificate>
#    <CertName>{cert_info["CertName"]}</CertName>
#    <CertType>TrustedCA</CertType>
#    <CertID>{cert_info["CertID"]}</CertID>
#    <CertValue>{cert_info["cert.hex"]}</CertValue>
#</SGDCertificate>\n""")
#    for cert_info in OCSP_Liste:
#        ausgabe += (f"""
#<SGDCertificate>
#    <CertName>{cert_info["CertName"]}</CertName>
#    <CertType>OCSPSigner</CertType>
#    <CertID>{cert_info["CertID"]}</CertID>
#    <CertValue>{cert_info["cert.hex"]}</CertValue>
#    <OCSPSigneeList>""")
#        assert len(CA_Liste)>0
#        for signee_info in CA_Liste:
#            ausgabe += (f"""
#        <OCSPSignee>
#            <CertName>{signee_info["CertName"]}</CertName>
#            <CertID>{signee_info["CertID"]}</CertID>
#        </OCSPSignee>""")
#
#        ausgabe += "\n    </OCSPSigneeList>\n</SGDCertificate>\n"
#
#logging.info(("In der TSL sind {} TSPs mit zusammen {} TSPServices enthalten." + 
#              " Es wurde{} {} Suchziel{} gefunden{}").format(
#      TSP_Counter, TSP_Service_Counter, 
#      '' if Zertifikate_Gefunden_Counter == 1 else 'n',
#      Zertifikate_Gefunden_Counter,
#      '' if Zertifikate_Gefunden_Counter == 1 else 'e',
#      ' und extrahiert' if Zertifikate_Gefunden_Counter > 0 else ''
#      ) + ".")
#
#ausgabe += "</SGDCertificateImportList>\n"
#
#print(ausgabe)
#
#
